from fastapi import FastAPI
from application import create_app


app = create_app()