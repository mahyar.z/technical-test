from tortoise.models import Model
from tortoise import fields


class Member(Model):
    """ Members """

    id = fields.IntField(pk=True, index=True)
    active = fields.BooleanField(default=True)
    created_at = fields.DatetimeField(auto_now_add=True, description="utc datetime on creation")
    modified_at = fields.DatetimeField(auto_now=True, description="utc datetime on modify")

    username = fields.CharField(max_length=128, unique=True, index=True)

    class Meta:
        table = "members"

    def __str__(self):
        return u"<Members:({}, {})>".format(self.id, self.username)