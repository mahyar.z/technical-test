from tortoise.models import Model
from tortoise import fields
from src.utils.enums import Access
from .member import Member


class Category(Model):
    """ Categories """

    id = fields.IntField(pk=True, index=True) 
    active = fields.BooleanField(default=True) 
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    name = fields.CharField(max_length=500, unique=True) 

    class Meta:
        table = "categories"

    def __str__(self):
        return u"<Categories:({}, {})>".format(self.id, self.name)   


class CategoryPermission(Model):
    """ Category Permissions """

    id = fields.IntField(pk=True, index=True) 
    active = fields.BooleanField(default=True) 
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    category = fields.ForeignKeyField('models.Category', related_name='category_permissions')
    member = fields.ForeignKeyField('models.Member', related_name='category_permissions')
    access = fields.CharEnumField(Access)

    class Meta:
        table = "category_permissions"

    def __str__(self):
        return u"<CategoryPermissions:({}, {}, {}, {})>".format(self.id,
                                                                self.member.username,
                                                                self.category.name,
                                                                self.access)
