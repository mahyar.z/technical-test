from tortoise.models import Model
from tortoise import fields
from src.utils.enums import Access


class Document(Model):
    """ Documents """

    id = fields.IntField(pk=True, index=True) 
    active = fields.BooleanField(default=True) 
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    author = fields.CharField(max_length=500, null=True) 
    subject = fields.TextField(null=False)
    content = fields.TextField(null=False)
    category = fields.ForeignKeyField('models.Category', related_name='documents', null=True)

    class Meta:
        table = "documents"

    def __str__(self):
        return u"<Documents:({}, {})>".format(self.id, self.subject) 


class DocumentPermission(Model):
    """ Document Permissions """

    id = fields.IntField(pk=True, index=True) 
    active = fields.BooleanField(default=True) 
    created_at = fields.DatetimeField(auto_now_add=True)
    modified_at = fields.DatetimeField(auto_now=True)

    document = fields.ForeignKeyField('models.Document', related_name='document_permissions')
    member = fields.ForeignKeyField('models.Member', related_name='document_permissions')
    access = fields.CharEnumField(Access, null=False)

    class Meta:
        table = "document_permissions"

    def __str__(self):
        return u"<DocumentPermissions:({}, {}, {}, {})>".format(self.id,
                                                                self.member.username,
                                                                self.document.name,
                                                                self.access)   

