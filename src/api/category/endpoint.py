from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from config import settings
from src.schemas.category import Category as CategorySchema
from src.schemas.category import CategoryPermission as CategoryPermissionSchema
from src.models.category import Category as CategoryModel
from src.models.category import CategoryPermission as CategoryPermissionModel
from .helper import set_category_permissions
from src.api.member.helper import populate_userinfo

router = APIRouter(prefix="/category")


@router.post("/")
async def create_category(category: CategorySchema):
    """
    Create category
    """

    model = CategoryModel(name=category.name)
    await model.save()

    return category


@router.put("/", dependencies=[Depends(populate_userinfo)])
async def update_category(category: CategorySchema):
    """
    Update category
    """

    return category


@router.get("/{id}", dependencies=[Depends(populate_userinfo)])
async def get_category(id: int):
    """
    Get category
    """

    category = await CategoryModel.get(pk=id)

    return category


@router.delete("/{id}", dependencies=[Depends(populate_userinfo)])
async def delete_category(id: int):
    """
    Delete category
    """

    category = await CategoryModel.get(pk=id).delete()

    return category


@router.post("/permission")
async def grant_category_permission(category_permission: CategoryPermissionSchema):
    """
    Grant category permission
    """

    result = await set_category_permissions(category_permission)

    return result