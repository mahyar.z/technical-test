from src.models.category import CategoryPermission as CategoryPermissionModel
from src.schemas.category import CategoryPermission as CategoryPermissionSchema
from src.models.category import Category as CategoryModel
from src.models.member import Member as MemberModel
from src.utils.exceptions import NotFound


async def set_category_permissions(category_permission: CategoryPermissionSchema):
    
    if not category_permission.accesses:
        raise NotFound("No access found!")

    await revoke_accesses(category_permission.member, category_permission.category)
    granted_access = [await set_access(member=category_permission.member, 
                                       category=category_permission.category, 
                                       access=access) for access in category_permission.accesses]
    
    # return granted accesses
    return [item for item in granted_access if item]


async def set_access(member, category, access):
    try:
        model = CategoryPermissionModel(member=await MemberModel.get(pk=member), 
                                        category=await CategoryModel.get(pk=category),
                                        access=access)

        await model.save()

    except Exception as e:
        return None

    return access

async def revoke_accesses(member, category):
    deleted = await CategoryPermissionModel.filter(member_id=member, category_id=category).delete()
    return deleted