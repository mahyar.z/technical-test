from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from config import settings
from src.schemas.member import Member as MemberSchema
from src.models.member import Member as MemberModel

router = APIRouter(prefix="/member")


@router.post("/")
async def create_member(member: MemberSchema):
    """
    Create member
    """

    model = MemberModel(username=member.username)
    await model.save()

    return member


@router.get("/{id}")
async def get_member(id: int):
    """
    Get member
    """

    member = await MemberModel.get(pk=id)

    return member


@router.delete("/{id}")
async def delete_member(id: int):
    """
    Delete member
    """

    member = await MemberModel.get(pk=id).delete()

    return member