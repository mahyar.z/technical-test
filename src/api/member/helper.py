from fastapi import Header, HTTPException


async def populate_userinfo(username: str = Header(...)):
    if not username:
        raise HTTPException(status_code=400, detail="username header is required!")

    return username