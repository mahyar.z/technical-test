from src.models.document import DocumentPermission as DocumentPermissionModel
from src.schemas.document import DocumentPermission as DocumentPermissionSchema
from src.models.document import Document as DocumentModel
from src.models.member import Member as MemberModel
from src.utils.exceptions import NotFound


async def set_document_permissions(document_permission: DocumentPermissionSchema):
    
    if not document_permission.accesses:
        raise NotFound("No access found!")

    await revoke_accesses(document_permission.member, document_permission.document)
    granted_access = [await set_access(member=document_permission.member, 
                                       document=document_permission.document, 
                                       access=access) for access in document_permission.accesses]
    
    # return granted accesses
    return [item for item in granted_access if item]


async def set_access(member, document, access):
    try:
        model = DocumentPermissionModel(member=await MemberModel.get(pk=member), 
                                        document=await DocumentModel.get(pk=document),
                                        access=access)

        await model.save()

    except Exception as e:
        return None

    return access


async def revoke_accesses(member, document):
    deleted = await DocumentPermissionModel.filter(member_id=member, document_id=document).delete()
    return deleted