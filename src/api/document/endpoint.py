from typing import Any, List
from fastapi import APIRouter, Body, Depends, HTTPException, Header
from fastapi.encoders import jsonable_encoder
from config import settings
from src.schemas.document import Document, DocumentPermission
from src.models.document import Document as DocumentModel
from .helper import set_document_permissions
from src.models.category import Category
from src.api.member.helper import populate_userinfo

router = APIRouter(prefix="/document")


@router.post("/")
async def create_document(document: Document):
    """
    Create document
    """

    model = DocumentModel(author=document.author, 
                          subject=document.subject,
                          content=document.content,
                          category=await Category.get(pk=document.category.id))

    await model.save()

    return document


@router.put("/", dependencies=[Depends(populate_userinfo)])
async def update_document(document: Document, username: str = Header(...)):
    """
    Update document
    """

    return document


@router.get("/{id}", dependencies=[Depends(populate_userinfo)])
async def get_document(id: int):
    """
    Get document
    """
    
    document = await DocumentModel.get(pk=id)

    return document


@router.delete("/{id}", dependencies=[Depends(populate_userinfo)])
async def delete_document(id: int):
    """
    Delete document
    """

    document = await DocumentModel.get(pk=id).delete()

    return document


@router.post("/permission")
async def grant_document_permission(document_permission: DocumentPermission):
    """
    Grant Document Permission
    """

    result = await set_document_permissions(document_permission)

    return result
