from enum import Enum


class Access(str, Enum):
    READ = "READ"
    CREATE = "CREATE"
    DELETE = "DELETE"
    UPDATE = "UPDATE"