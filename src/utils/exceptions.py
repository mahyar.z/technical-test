
class NotFound(Exception):
    pass

class AuthorizationError(Exception):
    pass