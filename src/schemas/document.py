from typing import List, Optional
from pydantic import BaseModel
from .category import CategoryRead as Category
from src.utils.enums import Access
from fastapi import Query


class Document(BaseModel):
    author: Optional[str] = Query(None, min_length=2, max_length=500)
    category: Optional[Category] = None
    subject: str
    content: str

class DocumentPermission(BaseModel):
    document: int = Query(..., title="The id of a document")
    member: int = Query(..., title="The id of a member")
    accesses: List[Access] = Query([], title="List of accesses")