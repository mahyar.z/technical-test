from pydantic import BaseModel
from src.utils.enums import Access
from fastapi import Query
from typing import List, Optional


class Category(BaseModel):
    name: str

class CategoryRead(BaseModel):
    id: int

class CategoryPermission(BaseModel):
    category: int = Query(..., title="The id of a category")
    member: int = Query(..., title="The id of a member")
    accesses: List[Access] = Query([], title="List of accesses")