from typing import Optional
from pydantic import BaseModel
from fastapi import Query


class Member(BaseModel):
    username: str = Query(..., min_length=3, max_length=128, title="Unique username for a member")