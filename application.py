
from fastapi import FastAPI
from config import settings
from fastapi import APIRouter
from importlib import import_module
from tortoise import Tortoise, run_async
from tortoise.contrib.fastapi import register_tortoise


async def on_startup():
    pass


async def on_shutdown():
    pass


def setup_router(app):
    from src.api import __all__ as blueprints

    api_router = APIRouter()

    for blueprint in blueprints:
        bp = import_module('src.api.%s' % blueprint, package=blueprint)
        for route in bp.__all__:
            api_router.include_router(getattr(bp, route))
        
    app.include_router(api_router, prefix=settings.API_STR)


def init_db(app):
    register_tortoise(app,
                    db_url=settings.DB_URL,
                    modules={"models": ["src.models"]},
                    add_exception_handlers=True)

    Tortoise.init_models(["src.models"], "models")


def create_app():

    app = FastAPI(
        title=settings.PROJECT_NAME, 
        on_startup=[on_startup],
        on_shutdown=[on_shutdown],
    )

    setup_router(app)
    init_db(app)

    return app