import secrets
from pydantic import BaseSettings, AnyHttpUrl
from typing import Any, Dict, List, Optional, Union

class Settings(BaseSettings):
    PROJECT_NAME: str = "TECHNICAL TEST"
    API_STR: str = "/api"
    SECRET_KEY: str = '59TpzL9Cvjft0_CYsBFMkwIqxME0Ey59vxN0Ow9a1Vc'
    TIMEOUT: int = 5
    DB_URL="postgres://auth:123456@127.0.0.1:5432/technical"
    
    class Config:
        env_file = ".env"
        case_sensitive = True

settings = Settings()